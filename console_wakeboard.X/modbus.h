#ifndef MODBUS__H
#define MODBUS__H

///////////////////////////////////////////////////////////////////////////////////////////////
// user data
///////////////////////////////////////////////////////////////////////////////////////////////
#define NULL 0
#define NUMBER_OF_DISCR_INPUTS 400
#define NUMBER_OF_INPUT_REGS   64
#define NUMBER_OF_OUTPUT_REGS  4

///////////////////////////////////////////////////////////////////////////////////////////////


#if NUMBER_OF_DISCR_INPUTS>400
#error wrong number of discrete inputs
#endif

#if NUMBER_OF_INPUT_REGS>256
#error wrong number of input registers
#endif

#define MB_BROADCAST_ADDR 0

#define MB_ERR_INVALID_FUNC	1
#define MB_ERR_INVALID_ADDR	2
#define MB_ERR_INVALID_DATA	3
#define MB_ERR_SLAVE_FAILURE	4


typedef enum {RX_Unknown, RX_Initial, RX_Idle, RX_Emission, RX_Reception, RX_Awaiting} e_MB_RX_State;
//�������������������������������������������������������������������������������Ŀ
//�            �RX_Unknown�RX_Initial�RX_Idle�RX_Emission�RX_Reception�RX_Awaiting�
//�������������������������������������������������������������������������������Ĵ
//�RX_Unknown  �          ��on init�۳       �           �            �           �
//�������������������������������������������������������������������������������Ĵ
//�RX_Initial  �          �          ��rx���۳           �            �           �
//�������������������������������������������������������������������������������Ĵ
//�RX_Idle     �          �          �       ��rx bad adr��rx normal�۳           �
//�������������������������������������������������������������������������������Ĵ
//�RX_Emission �          �          ��t35��۳�any rx���۳            �           �
//�������������������������������������������������������������������������������Ĵ
//�RX_Reception�          �          ��t35��۳�rx w err�۳            ��rx bad len�
//�������������������������������������������������������������������������������Ĵ
//�RX_Awaiting �          �          ��t35��۳           �            ��any rx���۳
//���������������������������������������������������������������������������������

typedef enum {Parity_None, Parity_Even, Parity_Odd} e_MB_Parity;
typedef enum {Frame_None, Frame_OK, Frame_NOK} e_MB_Flag_Frame;

typedef int (*MB_Function)();

#define START_T35() {TMR3ON=0; \
                     TMR3H=((char *) &MB_T35)[1]; \
                     TMR3L=MB_T35&0xff; \
                     TMR3IF=0; \
                     TMR3ON=1; }

extern const unsigned int crc16LUT[256];
extern unsigned char MB_Discrete_Inputs[NUMBER_OF_DISCR_INPUTS/8+1];
extern unsigned int MB_Input_Registers[NUMBER_OF_INPUT_REGS];
extern unsigned int MB_Output_Regisers[NUMBER_OF_OUTPUT_REGS];
extern unsigned int MB_Conn;
int MB_Init(unsigned char Addr, unsigned long speed, e_MB_Parity par);
void MB_Task();

#define	MB_FUNC_READ_COILS		0x01
#define	MB_FUNC_DISCRETE_INPUTS		0x02
#define MB_FUNC_READ_HOLDING_REGS       0x03
#define MB_FUNC_READ_INPUT_REGS		0x04
#define MB_FUNC_WRITE_SINGLE_COIL	0x05
#define MB_FUNC_WRITE_SINGLE_REG	0x06
#define MB_FUNC_READ_EXCEPTION_STATUS	0x07
#define MB_FUNC_DIAGNOSTICS		0x08
#define MB_FUNC_GET_COMM_EVENT_COUNTER	0x09
#define MB_FUNC_GET_COMM_EVENT_LOG	0x0c
#define MB_FUNC_WRITE_MPY_COILS		0x0f
#define MB_FUNC_WRITE_MPY_REGS		0x10
#define MB_FUNC_REPORT_SLAVE_ID		0x11

#endif
