/* 
 * File:   system.h
 * Author: user004
 *
 * Created on 28 ������� 2014 �., 9:43
 */

#ifndef SYSTEM_H
#define	SYSTEM_H

#include <xc.h>

#define DIR_A  0xFF
#define DIR_B  0xC0
#define DIR_C  0xC0

#define BTN_STOP PORTAbits.RA1
#define BTN_FWD  PORTAbits.RA2
#define BTN_REW  PORTAbits.RA3
#define BTN_MAN  PORTAbits.RA4
#define BTN_AUT  PORTAbits.RA5
#define BTN_SFWD PORTBbits.RB6
#define BTN_SREW PORTBbits.RB7

#define Switch   LATBbits.LATB5

#define SEC           1000
#define MB_TIME_CONN  (3*SEC)

#define ON              1
#define OFF             0
#define F_OSC           24000000

//#define TXD             0x00
//#define RXD             0x01
#define TXD             0x01
#define RXD             0x00


#define RS_232 0
#define RS_485 1
#define Mode_RS(bitnine)   (TX9D = bitnine?1:0;)

#define FastCRC16(crcData, crcReg) crcReg=((crcReg << 8) ^ crc16LUT[((char)(crcReg >> 8)) ^ crcData])

struct Port_B {
    unsigned         : 2;
    unsigned Out_LE  : 3;
    unsigned         : 3;
};
extern volatile struct Port_B Stbs @ 0xF8A;



void init_gpio();
void init_tmr0();
void init_tmr1();

extern const char Tabl[16];

typedef struct {
    unsigned workmode   : 8;
    unsigned command    : 8;
}TState;

extern TState StateWord;
typedef enum {WM_NONE=0, WM_AUTO, WM_MAN}eWorkMode;
typedef enum {DRV_NONE=0, DRV_FWD, DRV_REW, DRV_SLOW_FWD, DRV_SLOW_REW,DRV_STOP, DRV_SLOW_STOP=8} eDrive;

#endif	/* SYSTEM_H */
