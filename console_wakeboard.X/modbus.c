// intended to work on 1 uart
#include <pic18.h>
#include "modbus.h"
#include "rs232d.h"
#include "system.h"
#include "string.h"

const unsigned char lutCRCHi[] = {
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 
  0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
  0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
};

const char lutCRCLo[] = {
  0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 
  0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 
  0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 
  0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3, 0x11, 0xD1, 0xD0, 0x10, 
  0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 
  0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 
  0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 
  0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26, 0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 
  0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 
  0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 
  0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 
  0xB4, 0x74, 0x75, 0xB5, 0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 
  0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 
  0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 
  0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
  0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80, 0x40
};

e_MB_RX_State MB_RX_State=RX_Unknown;

e_MB_Flag_Frame MB_Flag_Frame;

unsigned int MB_T35, MB_T15,MB_prescaler,MB_cntprescaler;
e_MB_Parity MB_Parity;

char MB_My_Addr;

char MB_RX_Addr, MB_RX_Frame[255], MB_TX_Frame[255];
char *MB_RX_Frame_Pointer, *MB_TX_Frame_Pointer;
char MB_RX_Counter;
unsigned int MB_Conn;
bank0 char mb_parity_tmp, mb_parity_tmp_tx;

unsigned char MB_Discrete_Inputs[NUMBER_OF_DISCR_INPUTS/8+1];
unsigned int MB_Input_Registers[NUMBER_OF_INPUT_REGS];
unsigned int MB_Output_Regisers[NUMBER_OF_OUTPUT_REGS];

int Invalid_Function();
int Read_Discrete_Inputs();
int Read_Input_Registers();
int Write_Multiply_Registers();

const MB_Function MB_Functions[]={Invalid_Function,                  // 0x00 - ???
                                  Invalid_Function,                  // 0x01 - read coils
                                  Read_Discrete_Inputs,              // 0x02 - read discrete inputs
                                  Invalid_Function,                  // 0x03 - read holding regs
                                  Read_Input_Registers,              // 0x04 - read input regs
                                  Invalid_Function,                  // 0x05 - write single coil
                                  Invalid_Function,                  // 0x06 - write single reg
                                  Invalid_Function,                  // 0x07 - read exception status
                                  Invalid_Function,                  // 0x08 - diagnostics
                                  Invalid_Function,                  // 0x09 -
                                  Invalid_Function,                  // 0x0a -
                                  Invalid_Function,                  // 0x0b - get comm event counter
                                  Invalid_Function,                  // 0x0c - get comm event log
                                  Invalid_Function,                  // 0x0d -
                                  Invalid_Function,                  // 0x0e -
                                  Invalid_Function,                  // 0x0f - write multiply coils
                                  Write_Multiply_Registers,          // 0x10 - write multiply registers
                                  Invalid_Function,                  // 0x11 - report slave id
                                  Invalid_Function,                  // 0x12 -
                                  Invalid_Function,                  // 0x13 -
                                  Invalid_Function,                  // 0x14 - read file record
                                  Invalid_Function,                  // 0x15 - write file record
                                  Invalid_Function,                  // 0x16 - mask write register
                                  Invalid_Function,                  // 0x17 - read/write multiply registers
                                  Invalid_Function,                  // 0x18 - read fifo queue
                                  Invalid_Function,                  // 0x19 -
                                  Invalid_Function,                  // 0x1a -
                                  Invalid_Function,                  // 0x1b -
                                  Invalid_Function,                  // 0x1c -
                                  Invalid_Function,                  // 0x1d -
                                  Invalid_Function,                  // 0x1e -
                                  Invalid_Function,                  // 0x1f -
                                  Invalid_Function,                  // 0x20 -
                                  Invalid_Function,                  // 0x21 -
                                  Invalid_Function,                  // 0x22 -
                                  Invalid_Function,                  // 0x23 -
                                  Invalid_Function,                  // 0x24 -
                                  Invalid_Function,                  // 0x25 -
                                  Invalid_Function,                  // 0x26 -
                                  Invalid_Function,                  // 0x27 -
                                  Invalid_Function,                  // 0x28 -
                                  Invalid_Function,                  // 0x29 -
                                  Invalid_Function,                  // 0x2a -
                                  Invalid_Function,                  // 0x2b - Encapsulated transport support (some CANOpen funcs?)
};

MB_Function MB_Callback_Functions[sizeof(MB_Functions)/sizeof(MB_Function)]={NULL,};

interrupt void low_priority MB_isr (void) {
  static char RX_Byte, parity;
  static unsigned char CRCHi;
  static unsigned char CRCLo;
  static unsigned char adr;

  if (RCIF) {
    if (MB_RX_State!=RX_Unknown) {
      parity=RX9D;
      RX_Byte=RCREG;
      if (MB_Parity!=Parity_None) {
        mb_parity_tmp=RX_Byte;
        #asm
          BANKSEL _mb_parity_tmp
          swapf _mb_parity_tmp, W
          xorwf _mb_parity_tmp, F 
          rrcf  _mb_parity_tmp, W 
          xorwf _mb_parity_tmp, F
          btfsc _mb_parity_tmp, 2
          incf  _mb_parity_tmp, F
        #endasm
        if (MB_Parity==Parity_Odd) mb_parity_tmp^=1;
        parity=((mb_parity_tmp&1)==parity);
      }

      if (parity) {
        switch (MB_RX_State) {
          case RX_Idle      : MB_RX_Addr=RX_Byte;
                              if ((RX_Byte==MB_My_Addr)||(RX_Byte==MB_BROADCAST_ADDR)) {
                                MB_RX_Frame_Pointer=MB_RX_Frame;
                                MB_RX_Counter=0;
                                CRCHi=0xff;
                                CRCLo=0xff;

                                RX_Byte=CRCLo^RX_Byte;
                                CRCLo=CRCHi^lutCRCHi[RX_Byte];
                                CRCHi=lutCRCLo[RX_Byte];

                                MB_RX_State=RX_Reception;
                              } else {
                                MB_RX_State=RX_Emission;
                              }
          break;

          case RX_Reception : if (MB_RX_Counter<254) {
                                *MB_RX_Frame_Pointer=RX_Byte;
                                MB_RX_Frame_Pointer++;
                                MB_RX_Counter++;

                                RX_Byte=CRCLo^RX_Byte;
                                CRCLo=CRCHi^lutCRCHi[RX_Byte];
                                CRCHi=lutCRCLo[RX_Byte];
                              } else {
                                MB_RX_State=RX_Awaiting;
                              }

          break;

          default    : break;
        }
      } else {
       if (MB_RX_State!=RX_Initial) MB_RX_State=RX_Emission;
      }


      START_T35();
      MB_cntprescaler=MB_prescaler;
    }
  }

  if (TMR3IF) {
    TMR3IF=0;
    if(MB_cntprescaler>1){
        MB_cntprescaler--;
        START_T35();
    }else{
        if (MB_RX_State!=RX_Unknown) {
          TMR3ON=0;
          switch (MB_RX_State) {
            case RX_Initial   : MB_RX_State=RX_Idle;
            break;
            case RX_Reception : if ((MB_RX_Counter>2)&&((CRCHi|CRCLo)==0)) {
                                    MB_Flag_Frame=Frame_OK;
                                    MB_Conn=MB_TIME_CONN;
                                }else {
                                  MB_Flag_Frame=Frame_NOK;
                                  MB_RX_State=RX_Idle;
                                }
            break;
            case RX_Awaiting  : MB_Flag_Frame=Frame_NOK;
                                MB_RX_State=RX_Idle;
            break;
            case RX_Emission  : MB_RX_State=RX_Idle;
            break;
          }
        }
    }
  }
}

void MB_Send_Byte(char tx_byte) {
  if (MB_Parity!=Parity_None) {
    mb_parity_tmp_tx=tx_byte;
    #asm
      swapf _mb_parity_tmp_tx, W
      xorwf _mb_parity_tmp_tx, F 
      rrcf  _mb_parity_tmp_tx, W 
      xorwf _mb_parity_tmp_tx, F
      btfsc _mb_parity_tmp_tx, 2
      incf  _mb_parity_tmp_tx, F
    #endasm
  } else {
    mb_parity_tmp_tx=1;
  }
  while(!TRMT);
  TX9D=mb_parity_tmp_tx;
  TXREG=tx_byte;
}

void MB_Send_Frame(char len) {
  char CRCHi=0xFF, CRCLo=0xFF, tmp, i;

  Switch=TXD;
  if (MB_RX_Addr==MB_My_Addr) {
    MB_Send_Byte(MB_My_Addr);
    tmp=CRCLo^MB_My_Addr;
    CRCLo=CRCHi^lutCRCHi[tmp];
    CRCHi=lutCRCLo[tmp];

    for (i=0;i<len;i++) {
      tmp=MB_TX_Frame[i];
      MB_Send_Byte(tmp);
      tmp=CRCLo^tmp;
      CRCLo=CRCHi^lutCRCHi[tmp];
      CRCHi=lutCRCLo[tmp];
    }

    MB_Send_Byte(CRCLo);
    MB_Send_Byte(CRCHi);
  }

  while(!TRMT);
  Switch=RXD;
}

void MB_Task()
{
  int exception;
  char mb_func;
  MB_Function mb_func_ptr;

  if (MB_RX_State!=RX_Unknown) {
    if (MB_Flag_Frame==Frame_OK) {
      if (MB_RX_Counter>0) {
        mb_func=MB_RX_Frame[0];
        if (mb_func<(sizeof(MB_Functions)/sizeof(MB_Function))) {

          if (MB_Callback_Functions[mb_func]) {
            mb_func_ptr=MB_Callback_Functions[mb_func];
          } else {
            mb_func_ptr=MB_Functions[mb_func];
          }

          exception=mb_func_ptr();
          if (exception<0) {
            MB_TX_Frame[0]=mb_func|0x80;
            MB_TX_Frame[1]=-exception;
            MB_Send_Frame(2);
          } else {
            MB_TX_Frame[0]=mb_func;
            MB_Send_Frame(exception);
          }
        } else if (mb_func<0x80) {
            MB_TX_Frame[0]=mb_func|0x80;
            MB_TX_Frame[1]=-Invalid_Function();
            MB_Send_Frame(2);
        }
      }

      MB_Flag_Frame=Frame_None;
      MB_RX_State=RX_Idle;
    }

    if (MB_Flag_Frame==Frame_NOK) {
      MB_Flag_Frame=Frame_None;
    }
  }
}

void MB_Register_Callback_Function(char func_num, MB_Function func) {
  if (func_num) {
    MB_Callback_Functions[func_num]=func;
  }
}

int MB_Init(unsigned char Addr, unsigned long speed, e_MB_Parity par) {
  unsigned long tmp;

  CREN=0;
  TXEN=0;
  SPEN=0;
  RCIF=0;
  TMR3ON=0;

  /*MB_Input_Registers[0]=0x1234;
  MB_Input_Registers[1]=0x5678;
  MB_Input_Registers[2]=0x9abc;
  MB_Input_Registers[3]=0xdef0;

  MB_Discrete_Inputs[0]=0x11;
  MB_Discrete_Inputs[1]=0x22;
  MB_Discrete_Inputs[2]=0x33;
  MB_Discrete_Inputs[3]=0x44;
  MB_Discrete_Inputs[4]=0x55;
  MB_Discrete_Inputs[5]=0x66;
  MB_Discrete_Inputs[6]=0x77;
  MB_Discrete_Inputs[7]=0xff;*/

  MB_My_Addr=Addr;
  MB_RX_State=RX_Unknown;

  if(speed==1200){
    MB_prescaler=4;
    tmp=385*(F_OSC/4)/speed/10/MB_prescaler;      // T35 timeout is 11 bits*3.5=38.5=385/10
    MB_T35=65536-tmp;
  }else if(speed==2400){
    MB_prescaler=2;
    tmp=385*(F_OSC/4)/speed/10/MB_prescaler;      // T35 timeout is 11 bits*3.5=38.5=385/10
//    while(tmp>65535){
//        MB_prescaler++;
//        tmp=385*(F_OSC/4)/speed/10/MB_prescaler;      // T35 timeout is 11 bits*3.5=38.5=385/10
//    }
    MB_T35=65536-tmp;
  }else if (speed>=4800 && speed<=19200) {
    tmp=385*(F_OSC/4)/speed/10;      // T35 timeout is 11 bits*3.5=38.5=385/10
    if (tmp>65535) return -3;
    MB_T35=65536-tmp;
    MB_prescaler=1;
  } else {
    MB_T35=65536-(F_OSC/4000000)*1750; // if speed is higher than 19200 t35 timeout is fixed to 1750us (6mhz*1750us=10500)
    MB_prescaler=1;
  }

//  if (speed<=19200) {
//    tmp=165*(F_OSC/4)/speed/10;      // T15 timeout is 11 bits*1.5=16.5=165/10
//    if (tmp>65535) return -1;
//    MB_T15=65536-tmp;
//  } else {
//    MB_T15=65536-(F_OSC/4000000)*750;  // if speed is higher than 19200 t35 timeout is fixed to 750us
//  }

  MB_Parity=par;

  MB_RX_State=RX_Initial;

  Switch=RXD;
  init_serial_port(speed,RS_485);
  ADDEN = 0;
  T3CON  = 0x80;                  // enable timer writing with 1 16 bit operation
  TMR3IE = 1;

  START_T35();

  return 0;
}

/***************************************************************************************/
// utils
/***************************************************************************************/
char MB_Util_GetBits(char *array, unsigned int addr, char numbits) { // 0>num bits>=8
    unsigned int tmp;

    tmp = array[addr/8];
    tmp|= array[addr/8+1] << 8;
    tmp >>= addr%8;
    tmp &= 0xff>>(8-numbits);

    return tmp;
}

/***************************************************************************************/
// functions
/***************************************************************************************/
int Invalid_Function(){
  return -MB_ERR_INVALID_FUNC;
}

int Read_Discrete_Inputs(){
  unsigned int di_addr;
  int di_len;
  unsigned char cnt;


  if (MB_RX_Counter<7) return -MB_ERR_INVALID_FUNC;
  di_addr=(MB_RX_Frame[1]<<8)|MB_RX_Frame[2];
  di_len =(MB_RX_Frame[3]<<8)|MB_RX_Frame[4];

  if ((di_len<1)||(di_len>0x7d0)) return -MB_ERR_INVALID_DATA;
  if ((di_addr>=NUMBER_OF_DISCR_INPUTS)||((di_addr+di_len)>NUMBER_OF_DISCR_INPUTS)) return -MB_ERR_INVALID_ADDR;

  MB_TX_Frame[1]=di_len/8;             // qty of requsted inputs/8,
  if (di_len%8) MB_TX_Frame[1]++;      // if the remainder isn't zero inc value

  cnt=2;
  while (di_len>0) {
    if (di_len>8) MB_TX_Frame[cnt++]=MB_Util_GetBits(MB_Discrete_Inputs, di_addr, 8);
    else          MB_TX_Frame[cnt++]=MB_Util_GetBits(MB_Discrete_Inputs, di_addr, di_len);
    
    di_addr+=8;
    di_len-=8;
  }

  return MB_TX_Frame[1]+2;
}

int Read_Input_Registers(){
  unsigned int addr;
  int len;
  unsigned char cnt;

  if (MB_RX_Counter<7) return -MB_ERR_INVALID_FUNC;
  addr=(MB_RX_Frame[1]<<8)|MB_RX_Frame[2];
  len =(MB_RX_Frame[3]<<8)|MB_RX_Frame[4];

  if ((len<1)||(len>0x7d)) return -MB_ERR_INVALID_DATA;
  if ((addr>=NUMBER_OF_INPUT_REGS)||((addr+len)>NUMBER_OF_INPUT_REGS)) return -MB_ERR_INVALID_ADDR;

  MB_TX_Frame[1]=len*2;

  for (cnt=0;cnt<len;cnt++) {
    MB_TX_Frame[2+cnt*2]=MB_Input_Registers[addr+cnt]>>8;
    MB_TX_Frame[2+cnt*2+1]=(MB_Input_Registers[addr+cnt])&0xff;
  }

  return MB_TX_Frame[1]+2;
}

int Write_Multiply_Registers(){
  unsigned int addr,i,j;
  int len;
  unsigned char cnt, ByteCount;
  

  if(MB_RX_Counter<7) return -MB_ERR_INVALID_FUNC;
  addr=(MB_RX_Frame[1]<<8)|MB_RX_Frame[2];
  len =(MB_RX_Frame[3]<<8)|MB_RX_Frame[4];
  ByteCount = MB_RX_Frame[5];

  if (((len<1)||(len>0x7d))&&(len!=ByteCount*2)) return -MB_ERR_INVALID_DATA;
  if ((addr>=NUMBER_OF_INPUT_REGS)||((addr+len)>NUMBER_OF_INPUT_REGS)) return -MB_ERR_INVALID_ADDR;
  
  for(i=0,j=6;i<len;i++,j+=2){
    MB_Output_Regisers[addr+i]=MB_RX_Frame[j+1]|(MB_RX_Frame[j]<<8);
  }

  memcpy(&MB_TX_Frame[1],&MB_RX_Frame[1],4);
  return 5;
}
