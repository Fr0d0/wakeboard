#include <xc.h>
#include "system.h"
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "modbus.h"
#include "adc.h"

#define KOEFF_ADC   23.0//30.0//21.0
#define MAX_SPEED   36.6//28.0//40.0      // m/sec 
#define MAX_FREQ_MOTOR 6625


int tsec,tadc,tbtn;
unsigned char Ind[3],Cur_ind;
float speed,timeminute;
char err,dot;
int wtime;
char Prev_WorkMode;
char blink=1;
void SetInd(float val);

  
void interrupt isr(void){
    char i;
    if(TMR0IF){
        TMR0=0xffff-6000+1;
        if(tsec)tsec--;
        if(tadc)tadc--;
        if(tbtn)tbtn--;
        if(MB_Conn)MB_Conn--;
        i=Tabl[Ind[Cur_ind]];
        if(Cur_ind==1 && dot)i&=~0x80;  //point
        LATC=(LATC&0xc0)|(i&0x3f);
        LATB=(LATB&0xfc)|((i>>6)&0x03);
        Stbs.Out_LE=1<<Cur_ind;
        Cur_ind++;
        Cur_ind%=3;
        Stbs.Out_LE=0;
        TMR0IF=0;
    }
    if(ADIF){
        fAdc =1;
        ADIF = 0;
        ClrWdt();
    }
}

void main(){
   
    init_gpio();
    init_tmr0();
    IPEN=1;
    TMR0IP=1;
    ADIP=1;

    RCIP=0;
    TXIP=0;
    TMR3IP=0;
    MB_Init(2,19200,Parity_Even);
    initAdc();
    StateWord.command=DRV_STOP;
    while(1){
        MB_Task();
        if(fAdc && !tadc){
            fAdc=0;
            tadc=50;
            readAdc();
            speed=adcWord[0]/KOEFF_ADC;
            if(speed>MAX_SPEED)speed=MAX_SPEED;
            if(!err && !wtime) SetInd(speed);
        }
        if      (!BTN_AUT)StateWord.workmode=WM_AUTO;
        else if (!BTN_MAN)StateWord.workmode=WM_MAN;
        else              StateWord.workmode=WM_NONE;
        
       
        if((!BTN_FWD)
                /*&&(BTN_SFWD)
                &&(BTN_SREW)
                &&(StateWord.command!=DRV_SLOW_FWD)
                &&(StateWord.command!=DRV_SLOW_REW)
                /*&&(StateWord.command!=DRV_SLOW_STOP)*/) {
            StateWord.command=DRV_FWD;
            tbtn=1000;
        }
        if((!BTN_REW)
                /*&&(BTN_SFWD)
                &&(BTN_SREW)
                &&(StateWord.command!=DRV_SLOW_FWD)
                &&(StateWord.command!=DRV_SLOW_REW)
                /*&&(StateWord.command!=DRV_SLOW_STOP)*/){
            StateWord.command=DRV_REW;
            tbtn=1000;
        }
        if(Prev_WorkMode!=StateWord.workmode) {
            StateWord.command=DRV_STOP;
            Prev_WorkMode=StateWord.workmode;
        }
        if((StateWord.command==DRV_STOP||StateWord.command==DRV_NONE ||StateWord.command==DRV_SLOW_STOP)
                && !BTN_SFWD) StateWord.command=DRV_SLOW_FWD;
        if ((StateWord.command==DRV_STOP||StateWord.command==DRV_NONE ||StateWord.command==DRV_SLOW_STOP)
                && !BTN_SREW) StateWord.command=DRV_SLOW_REW;
        if ((StateWord.command==DRV_SLOW_FWD && BTN_SFWD)
                || (StateWord.command==DRV_SLOW_REW && BTN_SREW)) StateWord.command=DRV_SLOW_STOP;
        if((StateWord.command==DRV_FWD||StateWord.command==DRV_REW)&&!tbtn) 
            StateWord.command=DRV_NONE;
        if(!BTN_STOP) StateWord.command=DRV_STOP;
        MB_Input_Registers[0]=(speed*MAX_FREQ_MOTOR)/MAX_SPEED;
        memcpy((void*)&MB_Input_Registers[1],(void*)&StateWord,2);
        err=0,wtime=0;
        if(!MB_Conn) err=1;
        else if (MB_Output_Regisers[0]) err=(char)MB_Output_Regisers[0];
        if (MB_Output_Regisers[1]) wtime=MB_Output_Regisers[1];
        if(err || (wtime&&(StateWord.workmode==WM_AUTO))){
            if(!tsec){
                tsec=1000;
                if(blink){
                    memset(Ind,0x0f,3); //empty
                    blink=0;
                }else{
                    if(err){
                        Ind[0]=0x0e; //E
                        Ind[1]=0;    //0
                        Ind[2]=err;//numerror
                    }else if(wtime){
                        timeminute=wtime/60.0;
                        SetInd(timeminute);
                    }
                    blink=1;
                }
            }
        }else blink=1;

        if(!blink) dot=0;
        else if(err) dot=0;
        else if(wtime) dot=1;
        else dot=1;
    }
}

void SetInd(float val){
    Ind[0]=(unsigned char)(val/10);
    Ind[1]=(unsigned char)((val-Ind[0]*10));
    Ind[2]=(unsigned char)((val-((Ind[0]*10)+Ind[1]))*10);
}

