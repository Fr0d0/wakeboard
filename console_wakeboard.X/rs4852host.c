#include <pic18.h>
#include "def.h"
#include "crc.h"

#define ADDR_RS         0x01
#define DATA_RS         0x00

#define ERROR_TIME_OUT  -1
#define ERROR_CRC       -2
#define ERROR_CRC_POINT -3
#define ERROR_HARDWARE  -4

/* ���⢥ত���� ��� */
#define ACK         0x80
#define ERROR_RXD   0x81

bit Flag_Frame;
bit Start_Recive_Frame;

char Bufer_Frame[130];
char TX_Buf[16][132];
int pending=0, numseq, curb=0xff, tx_pos=0;
int AckStack[16]={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

char RX_Byte;
char Count_Char;
char Step_RXD_Frame;
char Step_TXD_Frame;
int Count_TimeOut;
int Number_Error;
unsigned int Length;
char OverloadCount;
//static unsigned int CRC1, rcvdCRC1;

void Transmita(char Dat);
char Transmit_Frame(char ackk,const char *Frame, char Len);
int f;
/***************************************************************/
/***************************************************************/
/***************************************************************/
void Transmita(char Dat)
{
    TXREG1 = Dat;
    while(!TRMT1);
}
/***************************************************************/
char Transmit_Frame(char ackk,const char *Frame, char Len)
{
    unsigned int Crc;
    unsigned char i, buf;

    buf=0xff;
    for (i=0;i<16;i++) 
    {
      if (((pending&((int)1<<i))==0) && (AckStack[i]==-1))
      {
        buf=i;
        OverloadCount=0;
        break;
      }
    }

    if (buf==0xff) {
       OverloadCount++;
       if(OverloadCount>=10)
         asm("reset");
       //RC0=!RC0;
       return 0;
    }



    Crc = 0;
    i=(ackk&0x0f)|(buf<<4);
    FastCRC16(Len+1,  Crc);
    FastCRC16(i, Crc);

    TX_Buf[buf][0]=Len+1;
    TX_Buf[buf][1]=i;
    for (i=0;i<Len;i++) {
       FastCRC16(Frame[i], Crc);
       TX_Buf[buf][i+2]=Frame[i];
    }          	
    TX_Buf[buf][Len+2]=Crc>>8;
    TX_Buf[buf][Len+3]=Crc&0xff;
    pending|=1<<buf;
    TX2IE=1;
    return 1;
}
/***************************************************************/
