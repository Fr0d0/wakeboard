#include "xc.h"
//#include "deviceDef.h"
//-------------------------------------------------------------------------------------
void initSlideAverage(void);
int slideAverage(char ch, int val);
void initAdc(void);
void readAdc(void);
//-------------------------------------------------------------------------------------

const char chanals[]={0x08,0x0c};

#define CLEAR_CHANAL ~0x38
#define LEN_AVR_BUF  8
#define MAX_CHANAL   1
//-------------------------------------------------------------------------------------
struct {
   char top;
   unsigned int  sum;
   unsigned int  buf[LEN_AVR_BUF];
} bufSlideAverage[MAX_CHANAL];
char curCh;
char cntAdcTimeOut;
char fAdc;
char cntRepeatConv;
int adcWord[MAX_CHANAL];
//-------------------------------------------------------------------------------------
void initSlideAverage(void) {
   char i,j;
   for (i=0;i<MAX_CHANAL;i++) {
      bufSlideAverage[i].sum=0;
      bufSlideAverage[i].top=0; 
      for (j=0;j<LEN_AVR_BUF;j++) bufSlideAverage[i].buf[j]=0;
   }
}//-------------------------------------------------------------------------------------
int slideAverage(char ch, int val) {
   bufSlideAverage[ch].sum -= bufSlideAverage[ch].buf[bufSlideAverage[ch].top];
   bufSlideAverage[ch].sum += val;
   bufSlideAverage[ch].buf[bufSlideAverage[ch].top]= val;
   if(++bufSlideAverage[ch].top==LEN_AVR_BUF) bufSlideAverage[ch].top=0;
   return(bufSlideAverage[ch].sum/LEN_AVR_BUF);
}//-------------------------------------------------------------------------------------
void initAdc(void) {
    ADCON0 = 0x01; //0 channel
    ADCON1 = 0x0e;
    ADCON2 = 0x82;
    ADIE = 1;
    PEIE = 1;
    ADIF = 0;
    fAdc = 0;
    initSlideAverage();
    curCh = 0;
    //cntAdcTimeOut = 0;
    GODONE = 1;
}//-------------------------------------------------------------------------------------
void readAdc(void) {
    int tmpAdcWord=0;
    *((char*)&tmpAdcWord+1) = ADRESH;
    *((char*)&tmpAdcWord)   = ADRESL;
    adcWord[curCh] = slideAverage(curCh,tmpAdcWord);
    curCh++;
    curCh %= MAX_CHANAL;

    ADCON0 |= 0x03;//(chanals[curCh]|0x03);
}//-------------------------------------------------------------------------------------
                                                	
