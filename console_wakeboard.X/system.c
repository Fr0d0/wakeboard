#include <xc.h>
#include "system.h"

const char Tabl[16]={0xc0, /*0*/
                     0xf9,
                     0xa4,
                     0xb0,
                     0x99, /*4*/
                     0x92,
                     0x82,
                     0xf8,
                     0x80, /*8*/
                     0x90,

                     0x88, /*a*/
                     0xc8, /*b==P(kyr.)*/
                     0xc6,
                     0xa1, /*d*/
                     0x86, /* E */
                     0xff}; /*empyness*/
                     /*0x8e}; */

typedef struct {
    unsigned workmode   : 8;
    unsigned command    : 8;
}TState;

TState StateWord;

volatile struct Port_B Stbs @ 0xF8A;



void init_gpio(){
    TRISA=DIR_A;
    TRISB=DIR_B;
    TRISC=DIR_C;
   // LATA=LATB=LATC=0;
    LATB=0xe0;
    LATC=0xc0;
    //ADCON1=0x0e; //an0 is analog
    CMCON=0x07;
    
    INTCONbits.PEIE=1;
    INTCONbits.GIE=1;
    INTCONbits.TMR0IF=0;
    INTCONbits.TMR0IE=1;
}


void init_tmr0(){
    TMR0=0xffff-6000+1;
    T0CON=0b10001000;
}


