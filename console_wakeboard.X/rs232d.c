#include "system.h"

void init_serial_port(unsigned long baud, unsigned char ninebits);

void
init_serial_port(unsigned long baud, unsigned char ninebits)
{
  int X;
  unsigned long tmp;

  /* calculate and set baud rate register */
  /* for asynchronous mode */
  tmp = 16UL * baud;
  X = (int)(F_OSC/tmp) - 1;
  if ((X>255) || (X<0)) {
    tmp = 64UL * baud;
    X = (int)(F_OSC/tmp) - 1;
    if (X > 255) X = 255;
    BRGH1 = 0;   /* low baud rate */
  }
  else BRGH1 = 1;   /* high baud rate */

  SPBRG1 = X;  /* set the baud rate */

  SYNC1 = 0;   /* asynchronous */
  SPEN = 1;   /* enable serial port pins */
  CREN = 1;   /* enable reception */
  SREN = 0;   /* no effect */
  TXIE = 0;   /* disable tx interrupts */
  RCIE = 1;   /* enable rx interrupts */
  TX91  = ninebits?1:0;    /* 8- or 9-bit transmission */
  RX9  = ninebits?1:0;    /* 8- or 9-bit reception */
  TXEN1 = 1;   /* enable the transmitter */
  PEIE = 1;
}

