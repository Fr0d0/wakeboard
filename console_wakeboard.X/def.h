#include <pic18.h>
 
#define CAN_CON1  0b00100001
#define CAN_STAT  0b01000000

#define ON              1
#define OFF             0
#define F_OSC           24000000

#define TXD             0x01
#define RXD             0x00

#define RS_232 0
#define RS_485 1
#define Mode_RS(bitnine)   (TX9D = bitnine?1:0;)

#define FastCRC16(crcData, crcReg) crcReg=((crcReg << 8) ^ crc16LUT[((char)(crcReg >> 8)) ^ crcData])

static volatile bit CAN1_CS @ ((unsigned)&PORTF*8)+4; // CAN1
static volatile bit CAN2_CS @ ((unsigned)&PORTF*8)+5; // CAN2
static volatile bit Switch @ (unsigned)&PORTC*8+0;
